use c9;

CREATE TABLE users (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(50) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL,
    role ENUM('Administrator', 'Donor', 'Pantry', 'Other'),
    first_name VARCHAR(20) DEFAULT 'N/A',
    last_name VARCHAR(20) DEFAULT 'N/A',
    organization VARCHAR(50) DEFAULT 'N/A',
    street VARCHAR(100) NOT NULL,
    city VARCHAR(50) NOT NULL,
    state CHAR(2) NOT NULL,
    zip_code CHAR(5) NOT NULL,
    phone VARCHAR(11) NOT NULL,
    email VARCHAR(50) NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)ENGINE=INNODB;

CREATE TABLE produce (
	name VARCHAR(25) NOT NULL PRIMARY KEY,
	units VARCHAR(20)
)ENGINE=INNODB;
	
CREATE TABLE donations (
	id INT(11) NOT NULL,
	produce VARCHAR(25) NOT NULL,
	quantity INT(11),
	data_entry_datetime TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	pick_up_date DATE,
	picked_up tinyint(1) DEFAULT 0,
	dropped_up tinyint(1) DEFAULT 0,
	PRIMARY KEY (id, produce, data_entry_datetime),
	INDEX produce_ind (produce),
	FOREIGN KEY (produce)
	    REFERENCES produce(name)
	    ON UPDATE CASCADE ON DELETE CASCADE,
	INDEX user_id (id),
	FOREIGN KEY (id)
	    REFERENCES users(id)
	    ON UPDATE CASCADE ON DELETE CASCADE
)ENGINE=INNODB;