use c9;

DELETE FROM donations WHERE id != 0;
DELETE FROM users WHERE username != '';
ALTER TABLE users AUTO_INCREMENT = 1; 

INSERT INTO users (username, password, role, first_name, last_name,
 organization, street, city, state, zip_code, phone, email)
VALUES ('admin', '$2y$10$JjCqm8qrHVwq.a8H0YPuy.lmLHCvvI6s7Y.9nqnT10etUE8b8Fuxq', 'Administrator', 'Admin', 'Istrator',
 'Seedfork of the Highlands', 'N/A',  'N/A', 'NA', '38587', '55555555', 'NA@NA.com');

INSERT INTO users (username, password, role, first_name, last_name,
 organization, street, city, state, zip_code, phone, email)
VALUES ('example_farmer', '$2y$10$JjCqm8qrHVwq.a8H0YPuy.lmLHCvvI6s7Y.9nqnT10etUE8b8Fuxq', 'Donor', 'example', 'user',
 'farmers inc', '123 street', 'city', 'TN',
'45678', '0123456789', 'farmer@example.com');

INSERT INTO users (username, password, role, first_name, last_name,
 organization, street, city, state, zip_code, phone, email)
VALUES ('FarmerJoe', '$2y$10$JjCqm8qrHVwq.a8H0YPuy.lmLHCvvI6s7Y.9nqnT10etUE8b8Fuxq', 'Donor', 'Joe', 'Schmoe',
 'JoeSchmoes', '555 not a real street dr.', 'SchmoesVille', 'TN',
'55555', '0123456789', 'joes@schmoes.com');

INSERT INTO users (username, password, role, first_name, last_name,
 organization, street, city, state, zip_code, phone, email)
VALUES ('FarmerJohn', '$2y$10$JjCqm8qrHVwq.a8H0YPuy.lmLHCvvI6s7Y.9nqnT10etUE8b8Fuxq', 'Donor', 'John', 'Smith',
 'JohnSmiths', '555 not a real street ln.', 'Smithsville', 'TN',
'55555', '0123456789', 'john@smiths.com');

INSERT INTO users (username, password, role, first_name, last_name,
 organization, street, city, state, zip_code, phone, email)
VALUES ('FarmerJane', '$2y$10$JjCqm8qrHVwq.a8H0YPuy.lmLHCvvI6s7Y.9nqnT10etUE8b8Fuxq', 'Donor', 'Jane', 'Smith',
 'JaneSmiths', '555 not a real street ln.', 'Smithsville', 'TN',
'55555', '0123456789', 'jane@smiths.com');

INSERT INTO users (username, password, role, first_name, last_name,
 organization, street, city, state, zip_code, phone, email)
VALUES ('CheerfulGiver001', '$2y$10$JjCqm8qrHVwq.a8H0YPuy.lmLHCvvI6s7Y.9nqnT10etUE8b8Fuxq', 'Donor', 'Humble', 'Joe',
 'GoodSamaritanInc', '0123 GivingStreet', 'PovertyLand', 'TN',
'45678', '0123456789', 'IWantForNothing@CharitableHeart.com');

INSERT INTO users (username, password, role, first_name, last_name,
 organization, street, city, state, zip_code, phone, email)
VALUES ('FarmerDude', '$2y$10$JjCqm8qrHVwq.a8H0YPuy.lmLHCvvI6s7Y.9nqnT10etUE8b8Fuxq', 'Donor', 'Jeremy', 'Love',
 'JL Farms', '555 Summergrove Ln', 'Cookeville', 'TN',
'38501', '0123456789', 'jeremylove@jlfarms.com');

INSERT INTO users (username, password, role, first_name, last_name,
 organization, street, city, state, zip_code, phone, email)
VALUES ('adamfrisk', '$2y$10$JjCqm8qrHVwq.a8H0YPuy.lmLHCvvI6s7Y.9nqnT10etUE8b8Fuxq', 'Donor', 'Adam', 'Frisk',
 'FriskyFarmers inc', '2710 Wales st', 'Metropolis', 'TN',
'55555', '0123456789', 'adam@friskyfarmers.com');

INSERT INTO users (username, password, role, first_name, last_name,
 organization, street, city, state, zip_code, phone, email)
VALUES ('brooklynfrisk', '$2y$10$JjCqm8qrHVwq.a8H0YPuy.lmLHCvvI6s7Y.9nqnT10etUE8b8Fuxq', 'Donor', 'Brooklyn', 'Frisk',
 'FriskyFarmers inc', '2710 Wales st', 'Metropolis', 'TN',
'55555', '0123456789', 'brooklyn@friskyfarmers.com');

INSERT INTO users (username, password, role, first_name, last_name,
 organization, street, city, state, zip_code, phone, email)
VALUES ('elijahduun', '$2y$10$JjCqm8qrHVwq.a8H0YPuy.lmLHCvvI6s7Y.9nqnT10etUE8b8Fuxq', 'Donor', 'Elijah', 'Dunn',
 'DunnNabIt inc', '1820 BallHollow Rd', 'Cookeville', 'TN',
'38501', '9315552903', 'elijah@dunnnabit.org');

INSERT INTO users (username, password, role, first_name, last_name,
 organization, street, city, state, zip_code, phone, email)
VALUES ('timkoger', '$2y$10$JjCqm8qrHVwq.a8H0YPuy.lmLHCvvI6s7Y.9nqnT10etUE8b8Fuxq', 'Donor', 'Timothy', 'Koger',
 'KogerKane inc', '2710 Sunset St', 'Pulaski', 'TN',
'38478', '9315551534', 'timothy@kogerkane.com');

INSERT INTO users (username, password, role, first_name, last_name,
 organization, street, city, state, zip_code, phone, email)
VALUES ('janerain', '$2y$10$JjCqm8qrHVwq.a8H0YPuy.lmLHCvvI6s7Y.9nqnT10etUE8b8Fuxq', 'Donor', 'Jane', 'Rain',
 'Watery Produce inc', '555 Riverrun Lake', 'Gotham', 'KY',
'55555', '93155551337', 'jane_rain@wateryproduce.com');

INSERT INTO users (username, password, role, first_name, last_name,
 organization, street, city, state, zip_code, phone, email)
VALUES ('juderain', '$2y$10$JjCqm8qrHVwq.a8H0YPuy.lmLHCvvI6s7Y.9nqnT10etUE8b8Fuxq', 'Donor', 'Jude', 'Rain',
 'Watery Produce inc', '555 Riverrun Lake', 'Gotham', 'KY',
'55555', '93155551337', 'jude_rain@wateryproduce.com');
	
INSERT INTO users (username, password, role, first_name, last_name,
 organization, street, city, state, zip_code, phone, email) 
VALUES ('FoodPantry45', '$2y$10$JjCqm8qrHVwq.a8H0YPuy.lmLHCvvI6s7Y.9nqnT10etUE8b8Fuxq','Pantry', 'Food', 'Pantry',
 'YourFriendlyNeighboorhoodFoodPantry', '1234 LocalStreet', 'Townsville', 'TN',
 '45678', '0010020003', 'WeGiveFood@localcharity.com');
	
INSERT INTO users (username, password, role, first_name, last_name,
 organization, street, city, state, zip_code, phone, email)
VALUES ('MisterPrez', '$2y$10$JjCqm8qrHVwq.a8H0YPuy.lmLHCvvI6s7Y.9nqnT10etUE8b8Fuxq', 'Other', 'Donald', 'Trump',
 'The White House', '1600 Pennsylvania Ave', 'NW',
 'Washington DC','20500', '2024566213', 'InsertWallJokeHere@YesThatIsTheActualPhoneNumber.com');


INSERT INTO users (username, password, role, first_name, last_name,
 organization, street, city, state, zip_code, phone, email) 
VALUES ('example_pantry', '$2y$10$JjCqm8qrHVwq.a8H0YPuy.lmLHCvvI6s7Y.9nqnT10etUE8b8Fuxq','Pantry', 'Food', 'Pantry',
 'pantry organization', '1234 pantry street', 'Townsville', 'TN',
 '45678', '0010020003', 'WeGiveFood@pantry.com');
