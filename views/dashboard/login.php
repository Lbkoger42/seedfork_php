<?php
// Include config file
require_once 'config.php';
 
// Define variables and initialize with empty values
$username = $password = $role = $id = "";
$username_err = $password_err = "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    // Check if username is empty
    if(empty(trim($_POST["username"]))){
        $username_err = 'Please enter username.';
    } else{
        $username = trim($_POST["username"]);
    }
    
    // Check if password is empty
    if(empty(trim($_POST['password']))){
        $password_err = 'Please enter your password.';
    } else{
        $password = trim($_POST['password']);
    }
    
    // Validate credentials
    if(empty($username_err) && empty($password_err)){
        // Prepare a select statement
        $sql = "SELECT id, username, password, role FROM users WHERE username = ?";
        
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_username);
            
            // Set parameters
            $param_username = $username;
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Store result
                mysqli_stmt_store_result($stmt);
                
                // Check if username exists, if yes then verify password
                if(mysqli_stmt_num_rows($stmt) == 1){                    
                    // Bind result variables
                    mysqli_stmt_bind_result($stmt, $id, $username, $hashed_password, $role);
                    if(mysqli_stmt_fetch($stmt)){
                        if(password_verify($password, $hashed_password)){
                            /* Password is correct, so start a new session and
                            save the username to the session */
                            session_start();
                            $_SESSION['username'] = $username;  
                            $_SESSION['role'] = $role;
                            $_SESSION['id'] = $id;
                            
                            header("location: index.php");
                        } else{
                            // Display an error message if password is not valid
                            $password_err = 'The password you entered was not valid.';
                            echo $hashed_password;
                        }
                    }
                } else{
                    // Display an error message if username doesn't exist
                    $username_err = 'No account found with that username.';
                }
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
        }
        
        // Close statement
        mysqli_stmt_close($stmt);
    }
    
    // Close connection
    mysqli_close($link);
}
?>

<html>
    <head>
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    </head>

    <body>
        <!--Navigation bar-->
        <div id="nav-placeholder">
        
        </div>
        
        <script>
            $(function(){
              $("#nav-placeholder").load("application.php");
            });
        </script>
        <!--end of Navigation bar-->  
        <div class="row">
            <div class="background col-md-12">
                <div class="logo">
                    <img src="/assets/images/seed.png" />
                </div>
            </div>
        </div>
        
        <div class="wrapper">
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                <div class="form-group <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>" id="user">
                    <input placeholder="Username" type="text" name="username"class="form-control" value="<?php echo $username; ?>">
                    <span class="help-block"><?php echo $username_err; ?></span>
                </div>    
                <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>" id="pass">
                    <input placeholder="Password" type="password" name="password" class="form-control">
                    <span class="help-block"><?php echo $password_err; ?></span>
                </div>
                <div class="form-group" id="loginbutt">
                    <input type="submit" class="btn btn-warning" value="Login">
                </div>
                <p id="signup">Don't have an account? <a href="sign_up.php">Sign up now</a>.</p>
            </form>
        </div>
        
        <style type="text/css">
            .wrapper{ width: 350px; padding: 20px; margin: 0 auto;}
            
            .about-content {
                margin-top: 1em;
            }
        
            .background {
                background-size: cover;
                background-position: center center;
                position: relative;
                height: 8vw;
                min-height: 250px;
                width: 100%;
            }
        
            .logo {
            }
        
                .logo > img {
                    min-width: 250px;
                    width: 27%;
                    left: 50%;
                    position: absolute;
                    transform: translateX(-50%);
                }
            
            #loginbutt {
                text-align: center;
            }
        
        
        </style>
    </body>
</html>