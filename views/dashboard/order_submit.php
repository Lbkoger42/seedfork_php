<!-- Start of access restriction -->
<?php
session_start();
if(strcmp($_SESSION['role'], "Administrator") != 0 && strcmp($_SESSION['role'], "Donor") != 0) {
    // does not have permission to access this page, redirect
    header('Location: no_access.php');
    die();
} else {
    // has permission, grant access
}
?>
<!-- End of access restriction -->

<html>
    <head>
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    </head>

    <body>
        <!--Navigation bar-->
        <div id="nav-placeholder">
        
        </div>
        
        <script>
            $(function(){
              $("#nav-placeholder").load("application.php");
            });
        </script>
        <!--end of Navigation bar-->  
    </body>
</html>

<html>
<body>

<?php
    require_once 'config.php';
    session_start();
    $username = $_SESSION['username'];
    $produce = $_POST['produce'];
    $quantity = $_POST['quantity'];
    //$units = $_POST['units'];
    date_default_timezone_set('America/Chicago');
    //$data_entry_datetime = new DateTime();
    $pickup_date = $_POST['pickup_date'];
    //$farmer_price = $_POST['farmer_price'];
    //$comments = $_POST['comments'];

    $date_str = 'next ' . $pickup_date;
    $date = date("Y-m-d",strtotime($date_str));
    //$price = money_format('%i', $farmer_price);
    
    $username = strtolower($username);
    $produce = strtolower($produce);
    //$units = strtolower($units);
    
    /*
    // temporary test stuff
    $username = 'username';
    echo "username: ", $username, "<br>";
    echo "produce: ", $produce, "<br>";
    echo "quantity: ", $quantity, "<br>";
    echo "units: ", $units, "<br>";
    //echo "data entry datetime: ", $data_entry_datetime->format('Y-m-d h:i:s a'), "<br>";
    $date = $date->format('Y-m-d');
    echo "pickup_date: ", $date, "<br>";
    echo "farmer_price: ", $price, "<br>";
    //echo "comments: ", $comments, "<br>";
    */
    
    // sql
    
    // get id
    /*
    $sql = "SELECT id FROM users WHERE username = '$username'";
    $result = mysqli_query($link, $sql);
    $row = mysqli_fetch_array($result);
    $user_id = $row['id'];
    */
    
    $user_id = "";
    if($_SESSION['role'] == ("Administrator")) {
        $user_id = $_POST['id'];
    } else {
        $user_id = $_SESSION['id'];
    }
    
    // validate that the produce and input exists in produce table
    $sql = "SELECT name FROM produce WHERE name = '$produce'";
    $result = mysqli_query($link, $sql);
    $row = mysqli_fetch_array($result);
    
    if ($row['name'] == $produce) {
//        $sql = "SELECT units FROM produce WHERE units = '$units'";
//        $result = mysqli_query($link, $sql);
    
//        $row = mysqli_fetch_array($result);
//        if ($row['units'] == $units) {
            // insert query into database
            $sql = "INSERT INTO donations (id, produce, quantity, pick_up_date) VALUES (?, ?, ?, ?)";
            
            if($stmt = mysqli_prepare($link, $sql)){
                // Bind variables to the prepared statement as parameters
                mysqli_stmt_bind_param($stmt, "isis", $param_user_id, $param_produce, $param_quantity, $param_date);
                
                $param_user_id = $user_id;
                $param_produce = $produce;
                $param_quantity = $quantity;
                $param_date = $date;
                
                if(mysqli_stmt_execute($stmt)){
                    // Redirect to login page
                    //header("location: login.php");
                    echo '<p>Donation submitted.</p>';
                } else{
                    echo '<p>Something went wrong. Please try again later.</p>';
                }
            }
            
            
            //$sql = "INSERT INTO donations (id, produce, quantity, pick_up_date, picked_up, dropped_off) 
            //VALUES ('$user_id', '$produce', '$quantity', '$date', DEFAULT, DEFAULT)";
            
            //mysqli_query($link, $sql)/* or die('Error querying database.')*/;
            //echo '<p>Donation submitted.</p>';
//        } else {
//            echo '<p>Error with submission. The unit for this produce was not found.<p>';
//        }
    } else {
        echo '<p>Error with submission. The produce was not found.<p>';
    }
    
    mysqli_close($link);
?>

</body>
</html>