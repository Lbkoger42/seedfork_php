<!--

About (informational) page.

-->
<html>
    
    <head>
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    </head>
    <body>
        <!--Navigation bar-->
        <div id="nav-placeholder">
        
        </div>
        
        <script>
            $(function(){
              $("#nav-placeholder").load("application.php");
            });
        </script>
        <!--end of Navigation bar-->
        <div class="row">
            <div class="background col-md-12">
                <div class="logo">
                    <img src="/assets/images/seed.png" />
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="header col-md-12">
                    <h1>Our origins.</h1>
                </div>
            </div>
        </div>
        
        <div class="container-fluid" id="video-content">
            <div class="row">
                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="embed-responsive embed-responsive-16by9" id="ellen">
                          <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/r7UB4S8IF9I"></iframe>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="embed-responsive embed-responsive-16by9" id="sfh">
                          <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/HkjuEv6Wxcw"></iframe>
                    </div>
                </div>
                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <h3 >In the dead of winter while frozen soil waited for spring,
                    seeds were germinating in the minds of fund raiser Sarah Winfree, 
                    farmer Randy Dodson, Chef Rodney Laulo, and horticultural therapist
                    Ellen Wolfe. Each had a vision to bring locally grown food, local people,
                    and local restaurant chefs together in an event that would support healthier 
                    local communities. The image of seating people together at long tables to enjoy 
                    locally grown food prepared by local chefs in the great outdoors grew into a nurturing
                    team of people to include Eight Farms, Four local restaurants, over 40 sponsors, and 
                    dozens of volunteers. As farmers and chefs combined skills and creativity for
                    growing and preparing foods, sponsors graciously supported an effort aimed
                    to educate people to eat well and be healthy.</h3>
                </div>
                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12"></div>
            </div>
        </div>
        
        <style type="text/css">
        
            .about-content {
                margin-top: 1em;
            }
            
            .header h1 {
            }
            
            .row {
                width: calc(100% - 30px);
            }
        
            .background {
                padding-top: 25px;
                background-size: cover;
                background-position: center center;
                position: relative;
                height: 8vw;
                min-height: 150px;
                width: 100%;
            }
            .logo {
            }
        
                .logo > img {
                    min-width: 250px;
                    width: 27%;
                    left: 50%;
                    position: absolute;
                    transform: translateX(-50%);
                }
        
            .header h1 {
                font-size: 4.15vw; /*control size of statement*/
                color: black;
                font-family: "Segoe UI Light";
                font-weight: 100;
                letter-spacing: -1px;
                text-align: center;
                opacity: 1;
                padding-bottom: 25px;
            }
        
            h3 {
                margin-top: 80px;
            }
            
            .video-content ellen{
                padding-bottom: 20px;
            }
            
            .video-content sfh{
                padding-top: 200px;
            }
        </style>
    </body>
</html>