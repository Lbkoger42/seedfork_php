<!-- Start of access restriction -->
<?php
session_start();
if(strcmp($_SESSION['role'], "Administrator") != 0) {
    // does not have permission to access this page, redirect
    header('Location: no_access.php');
    die();
} else {
    // has permission, grant access
}
?>
<!-- End of access restriction -->

<html>
    <head>
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    </head>

    <body>
        <!--Navigation bar-->
        <div id="nav-placeholder">
        
        </div>
        
        <script>
            $(function(){
              $("#nav-placeholder").load("application.php");
            });
        </script>
        <!--end of Navigation bar-->  
    </body>
</html>

<!doctype html>
<form method="post" action="produce_submit.php">
    <div class="row form-group home-content">
        <div class"col-md-12" id="ebase"></div>
    </div>
    
    <!-- produce -->
    <div class="row form-group home-content" id="e1">
        <div class="col-lg-4 col-md-2"></div>
        <div class="col-lg-4 col-md-4 col-sm-12">
            <input placeholder="Type of produce" class="form-control" name="produce" type="text" required>
        </div>
        <div class="col-lg-4 col-md-2"></div>
    </div>
    
    <!-- units -->
    <div class="row form-group home-content" id="e1">
        <div class="col-lg-4 col-md-2"></div>
        <div class="col-lg-4 col-md-4 col-sm-12">
            <input placeholder="Units" class="form-control" name="units" type="text" required>
        </div>
        <div class="col-lg-4 col-md-2"></div>
    </div>
    
    <!-- submit button -->
    <div class="row form-group home-content" id="e5">
        <div class="col-md-4"></div>
        <div class="col-md-1">
            <input type="submit">
        </div>
        <div class="col-md-3"></div>
        <div class="col-md-4"></div>
    </div>
</form>
<style type="text/css">
    
    ebase {
        background-color: black;
    }
    
    e1 {
    }
    
    e2 {
    }
    
    e3 {
        
    }
    
</style>