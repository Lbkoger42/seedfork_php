<?php
//include database configuration file
require_once 'config.php';

// Create connection
$link  = new mysqli($servername, $username, $password, $database, $dbport);

// Check connection
if ($link ->connect_error) {
    die("Connection failed: " . $link ->connect_error);
}

//get records from database
//$query = $db->query("SELECT * FROM donations");
$sql = "SELECT * FROM donations";
mysqli_query($link, $sql) or die('Error querying database.');
$query = mysqli_query($link, $sql);

if($query->num_rows > 0){
    $delimiter = ",";
    $filename = "donations_" . date('Y-m-d') . ".csv";
    
    //create a file pointer
    $f = fopen('php://memory', 'w');
    
    //set column headers
    $fields = array('Id', 'Prodce', 'Quantity', 'Data Entry Time', 'Pick Up Date', 'Picked Up', 'Dropped Off');
    fputcsv($f, $fields, $delimiter);
    
    //output each row of the data, format line as csv and write to file pointer
    while($row = $query->fetch_assoc()){
        $picked_up = ($row['picked_up'] == '1')?'Yes':'No';
        $dropped_off = ($row['dropped_off'] == '1')?'Yes':'No';
        $lineData = array($row['id'], $row['produce'], $row['quantity'], $row['data_entry_datetime'], $row['pick_up_date'], $picked_up, $dropped_off);
        fputcsv($f, $lineData, $delimiter);
    }
    
    //move back to beginning of file
    fseek($f, 0);
    
    //set headers to download file rather than displayed
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '";');
    
    //output all remaining data on a file pointer
    fpassthru($f);
}
exit;

?>