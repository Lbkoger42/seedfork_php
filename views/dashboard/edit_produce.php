<!-- Start of access restriction -->
<?php
session_start();
if(strcmp($_SESSION['role'], "Administrator") != 0 && strcmp($_SESSION['role'], "Donor") != 0) {
    // does not have permission to access this page, redirect
    header('Location: no_access.php');
    die();
} else {
    // has permission, grant access
    
    require_once 'config.php';
}
?>
<!-- End of access restriction -->

<html>
    <head>
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    </head>

    <body>
        <!--Navigation bar-->
        <div id="nav-placeholder">
        
        </div>
        
        <script>
            $(function(){
              $("#nav-placeholder").load("application.php");
            });
        </script>
        <!--end of Navigation bar-->

<?php
    
    if(isset($_GET['name']))
    {
        $id = $_GET['name'];
        $sql = "SELECT * FROM produce WHERE name='$id'";
        mysqli_query($link, $sql) or die('Error querying database.');

        $result = mysqli_query($link, $sql);
        $row = mysqli_fetch_array($result);
    }
    else{
        echo 'ERROR!';
    }
    //echo $_GET['name'];
    /*
    if(isset($_GET['units'])){
        echo $_GET['units'];
    }
    else{
        echo 'that did not work';
    }*/
?>

    <div class="row" style="min-height: 20px">
    </div>

    <div class="row">
        <div class="col-lg-4"></div>
        <div class="col-lg-2 col-sm-12 col-xs-12">
            <form action = "edit_produce_entry.php" method = "POST">
                        
            <input type="hidden" name="id" value="<?php echo $id; ?>"/>
            <label for="newproduce" class="col-sm-4 control-label">Name: </label>
                <div class="col-sm-8" style="max-width: 250px; padding-bottom:5px;">
                    <input type = "text" class="form-control" name = "newname" value = "<?php echo $row['name']; ?>">
                </div>
            <label for="newproduce" class="col-sm-4 control-label">Units: </label>
                <div class="col-sm-8" style="max-width: 250px; padding-bottom:5px;">        
                    <input type = "text" class="form-control" name = "newunits" value = "<?php echo $row['units']; ?>">
                </div>
                <div class="row">
                    <div class="col-lg-5"></div>
                    <div class="col-lg-2">
                        <input type = "submit" value = "submit" name = "submit">
                    </div>
                    <div class="col-lg-5"></div>
                </div>
            </form>
        </div>
        <div class="col-lg-6"></div>
    </div>
    </body>
</html>