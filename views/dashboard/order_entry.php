<!-- Start of access restriction -->
<?php
session_start();
if(strcmp($_SESSION['role'], "Administrator") != 0 && strcmp($_SESSION['role'], "Donor") != 0) {
    // does not have permission to access this page, redirect
    header('Location: no_access.php');
    die();
} else {
    // has permission, grant access
}
?>
<!-- End of access restriction -->

<html>
    <head>
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    </head>

    <body>
        <!--Navigation bar-->
        <div id="nav-placeholder">
        
        </div>
        
        <script>
            $(function(){
              $("#nav-placeholder").load("application.php");
            });
        </script>
        <!--end of Navigation bar-->  
    </body>
</html>

<!doctype html>
<form method="post" action="order_submit.php">
    <div class="row form-group home-content">
        <div class"col-md-12" id="ebase"></div>
    </div>
    
    <?php
    if($_SESSION['role'] == ("Administrator")) { 
        echo '<div class="row form-group home-content" id="e1">';
            echo '<div class="col-lg-4 col-md-2"></div>';
            echo '<div class="col-lg-4 col-md-4 col-sm-12">';
                echo '<input placeholder="User ID" class="form-control" name="id" type="text" required>';
            echo '</div>';
            echo '<div class="col-lg-4 col-md-2"></div>';
        echo '</div>';
    }
    ?>
    
    <!-- produce -->
    <div class="row form-group home-content" id="e2">
        <div class="col-lg-4 col-md-2"></div>
        <div class="col-lg-4 col-md-4 col-sm-12">
            <input placeholder="Type of produce" class="form-control" name="produce" type="text" required>
        </div>
        <div class="col-lg-4 col-md-2"></div>
    </div>
    
    <!-- amount of produce & price -->
    <div class="row form-group home-content" id="e3">
        <div class="col-lg-4 col-md-2"></div>
        <!--
        <div class="col-lg-2 col-md-4 col-sm-12">
            <input placeholder="Units" class="form-control" name="units" type="text" required>
        </div>
        -->
        <!--
        <div class="col-lg-2 col-md-4 col-sm-12">
            <input placeholder="Price per unit" class="form-control" name="farmer_price" type="text" required>
        </div>
        -->
        <div class="col-lg-4 col-md-4 col-sm-12">
            <input placeholder="Amount of produce" class="form-control" name="quantity" type="text" required>
        </div>
        <div class="col-lg-4 col-md-2"></div>
    </div>
    
    <!-- time & place information -->
    <div class="row form-group home-content" id="e4">
        <div class="col-lg-4 col-md-2"></div>
        <!--
        <div class="col-lg-3 col-md-4 col-sm-12">
            <input placeholder="Pickup location" class="form-control" name="location" type="text" required>
        </div>
        -->
        <!--
        <div class="col-lg-1 col-md-2 col-sm-12">
            <input placeholder="Pickup date" class="form-control" id="pickup_date" type="text" required>
        </div>
        -->
        <!--
        this is a drop-down list of the days that produce is able to be picked up
        the selection is parsed so that the next calendar day that is one of the days in the list is submitted to the database
        ex: today is 3/21/2018, tuesday is selected
            the next tuesday - 3/27/2018 - is put into the database
        -->
        <div class="col-lg-3 col-md-2 col-sm-12">
            Select a pickup day: <select name="pickup_date">
                <option value="Tuesday">Tuesday</option>
                <option value="Thursday">Thursday</option>
            </select>
        </div>
        <div class="col-lg-4 col-md-2"></div>
    </div>
    
    <!-- additional comments -->
    <!--
    <div class="row form-group home-content" id="e4">
        <div class="col-lg-4 col-md-2"></div>
        <div class="col-lg-4 col-md-4 col-sm-12">
            <input placeholder="Comments" class="form-control" name="comments" type="text">
        </div>
        <div class="col-lg-4 col-md-2"></div>
    </div>
    -->
    
    <!-- submit button -->
    <div class="row form-group home-content" id="e5">
        <div class="col-md-4"></div>
        <div class="col-md-1">
            <input type="submit">
        </div>
        <div class="col-md-3"></div>
        <div class="col-md-4"></div>
    </div>
</form>
<style type="text/css">
    
    ebase {
        background-color: black;
    }
    
    e1 {
    }
    
    e2 {
    }
    
    e3 {
        
    }
    
</style>