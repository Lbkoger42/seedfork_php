<?php
    // A simple PHP script demonstrating how to connect to MySQL.
    // Press the 'Run' button on the top to start the web server,
    // then click the URL that is emitted to the Output tab of the console.

    $servername = getenv('IP');
    $username = getenv('C9_USER');
    $password = "";
    $database = "c9";
    $dbport = 3306;

    // Create connection
    $link  = new mysqli($servername, $username, $password, $database, $dbport);

    // Check connection
    if ($link ->connect_error) {
        die("Connection failed: " . $link ->connect_error);
    } 
?>