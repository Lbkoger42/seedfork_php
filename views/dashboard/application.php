<!DOCTYPE html>
<?php session_start(); ?>
<html lang="en">
  <head>
    <title>SeedforkWebsite</title>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </head>

  <body>
    <nav class="navbar navbar-default navbar-custom">
      <div class="container-fluid"> <!--open container-->
        
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#defaultNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>                        
          </button>
        </div>
        
        <div class="collapse navbar-collapse" id="defaultNavbar">
          <ul id="buttons" class="nav navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="index.php">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="about.php">About</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="contact.php">Contact</a>
            </li>
            <?php
            if(strcmp($_SESSION['role'], "Administrator") == 0 || strcmp($_SESSION['role'], "Donor") == 0) {
                echo '<li class="nav-item"><a class="nav-link" href="order_entry.php">Submit Donations</a></li>';
            }
            ?>
            <?php
            if(strcmp($_SESSION['role'], "Administrator") == 0 || strcmp($_SESSION['role'], "Donor") == 0 || strcmp($_SESSION['role'], "Pantry") == 0) {
              echo '<li class="nav-item"><a class="nav-link" href="view_donations.php">View Pending Donations</a></li>';
            }
            ?>
            <?php
            if(strcmp($_SESSION['role'], "Administrator") == 0) {
              echo '<li class="nav-item"><a class="nav-link" href="produce_entry.php">Submit Produce</a></li>';
            }
            ?>
            <?php
            if(strcmp($_SESSION['role'], "Administrator") == 0) {
              echo '<li class="nav-item"><a class="nav-link" href="view_produce.php">View Produce</a></li>';
            }
            ?>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <?php
            //echo $_SESSION['username'];
              if(strcmp($_SESSION['role'], "Administrator") != 0 && strcmp($_SESSION['role'], "Donor") != 0 && strcmp($_SESSION['role'], "Pantry") != 0) {
                echo '<li><a href="sign_up.php"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>';
              }
            ?>
            <?php
             if(strcmp($_SESSION['role'], "Administrator") != 0 && strcmp($_SESSION['role'], "Donor") != 0 && strcmp($_SESSION['role'], "Pantry") != 0) {
                echo '<li><a href="login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>';
              }
            ?>
            <?php
             if(strcmp($_SESSION['role'], "Administrator") == 0 || strcmp($_SESSION['role'], "Donor") == 0 || strcmp($_SESSION['role'], "Pantry") == 0) {
               echo '<li><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>';
             }
            ?>
          </ul>
        </div>
      </div> <!--close container-->
    </nav>
  </body>
</html>

<style type="text/css">
  .navbar-custom {
    color: black;
    background-color: white;
    
  }
  
  .navbar-default .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:focus {
    background-color: #aaaaa8;
    color: black;
  }
</style>