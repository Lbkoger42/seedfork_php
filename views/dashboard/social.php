<html>
    <head>
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    </head>

    <body>
        <!--Navigation bar-->
        <div id="nav-placeholder">
        
        </div>
        
        <script>
            $(function(){
              $("#nav-placeholder").load("application.php");
            });
        </script>
        <!--end of Navigation bar-->  
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <a class="twitter-timeline" href="https://twitter.com/SeedForkHiLands?ref_src=twsrc%5Etfw">
                    Tweets by SeedForkHiLands
                </a>
                <script async src="https://platform.twitter.com/widgets.js" charset="utf-8">
                    
                </script>
            </div>
            <div class="col-md-3"></div>
        </div>
    </body>
</html>