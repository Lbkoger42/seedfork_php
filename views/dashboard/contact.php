<!--

Contact information page.

-->
<!--
@{
    ViewData["Title"] = "Contact";
}

<h2>@ViewData["Title"]</h2>
-->
<html>
    <head>
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    </head>
    <body>
        <!--Navigation bar-->
        <div id="nav-placeholder">
        
        </div>
        
        <script>
            $(function(){
              $("#nav-placeholder").load("application.php");
            });
        </script>
        <!--end of Navigation bar-->  
        <div class="row">
            <div class="background col-md-12">
                <div class="logo">
                    <img src="/assets/images/seed.png" />
                </div>
            </div>
        </div>
        
        <address>
            Cookeville<br />
            Tennessee 38506<br />
            P: 931.372.5555
        </address>
        
        <address>
            <strong>Support:</strong> <a href="mailto:Support@example.com">Support@example.com</a><br />
            <strong>Marketing:</strong> <a href="mailto:Marketing@example.com">Marketing@example.com</a>
        </address>
        
        <style type="text/css">
        
            .row {
                width: calc(100% - 30px);
            }
        
            .about-content {
                margin-top: 1em;
            }
        
            h2 {
                text-align: center;
            }
        
            .background {
                background-size: cover;
                background-position: center center;
                position: relative;
                height: 8vw;
                min-height: 125px;
                width: 100%;
            }
        
            .logo {
            }
        
                .logo > img {
                    min-width: 250px;
                    width: 27%;
                    left: 50%;
                    position: absolute;
                    transform: translateX(-50%);
                }
        
            address {
                font-size: 20px;
                color: black;
                font-family: "Segoe UI Light";
                font-weight: 100;
                letter-spacing: -1px;
                text-align: center;
                opacity: 1;
            }
        </style>
    </body>
</html>