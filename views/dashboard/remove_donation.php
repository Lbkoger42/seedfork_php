<!-- Start of access restriction -->
<?php
session_start();
if(strcmp($_SESSION['role'], "Administrator") != 0 && strcmp($_SESSION['role'], "Donor") != 0) {
    // does not have permission to access this page, redirect
    header('Location: no_access.php');
    die();
} else {
    // has permission, grant access
}
?><!-- End of access restriction --><?php
require_once 'config.php';

if(isset($_GET['id']) && isset($_GET['produce']) && isset($_GET['data_entry'])){
    $id = $_GET['id'];
    $produce = $_GET['produce'];
    $data_entry = $_GET['data_entry'];
    
    $sql = "DELETE FROM donations WHERE id = '$id' AND produce = '$produce' AND data_entry_datetime = '$data_entry'";
    mysqli_query($link, $sql) or die('Error deleting from database.');
    header('Location: view_donations.php');
    }
else{
    echo "You have entered this page by accident.";
}



?>
