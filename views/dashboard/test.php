<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style type="text/css">
.dropbtn {
    background-color: white;
    color: black;
    padding: 16px;
    font-size: 16px;
    border: none;
}

.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f1f1f1;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

.dropdown-content a:hover {background-color: #ddd}

.dropdown:hover .dropdown-content {
    display: block;
}

.dropdown:hover .dropbtn {
    background-color: #aaaaa8;
    color: black;
}
</style>
</head>
<body>



<?php
if($_SESSION['role'] == ("Administrator" or "Donor")) {
    echo '<div class="dropdown">';
      echo '<button class="dropbtn">Donations</button>';
      echo '<div class="dropdown-content">';
        echo '<li><a href="order_entry.php"><span class="glyphicon glyphicon-user"></span>Submit Donations</a></li>';
        echo '<li class="nav-item"><a class="nav-link" href="view_donations.php">View Donations</a></li>';
        echo '<li class="nav-item"><a class="nav-link" href="produce_entry.php">Submit Produce</a></li>';
      echo '</div>';
    echo '</div>';
}
?>

</body>
</html>
