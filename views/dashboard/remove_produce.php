<!-- Start of access restriction -->
<?php
    session_start();
    if(strcmp($_SESSION['role'], "Administrator") != 0 && strcmp($_SESSION['role'], "Donor") != 0) {
        // does not have permission to access this page, redirect
        header('Location: no_access.php');
        die();
    } else {
        // has permission, grant access
    }

    require_once 'config.php';
    
    if(isset($_GET['name'])){
        $id = $_GET['name'];
        $sql = "delete from produce WHERE name = '$id'";
        $result = mysqli_query($link, $sql) or die('Error deleting from database.');
        header("location: view_produce.php");
    }
    else{
        echo "You have entered this page by accident.";
    }
    
    
?>