<!-- Start of access restriction -->
<?php
session_start();
if(strcmp($_SESSION['role'], "Administrator") != 0) {
    // does not have permission to access this page, redirect
    header('Location: no_access.php');
    die();
} else {
    // has permission, grant access
}
?>
<!-- End of access restriction -->

<html>
    <head>
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    </head>

    <body>
        <!--Navigation bar-->
        <div id="nav-placeholder">
        
        </div>
        
        <script>
            $(function(){
              $("#nav-placeholder").load("application.php");
            });
        </script>
        <!--end of Navigation bar-->  
    </body>
</html>

<?php
    //include database configuration file
    require_once 'config.php';

    // Create connection
    $link  = new mysqli($servername, $username, $password, $database, $dbport);

    // Check connection
    if ($link ->connect_error) {
        die("Connection failed: " . $link ->connect_error);
    }
    
    $produce = $_POST['produce'];
    $units = $_POST['units'];
    
    $produce = strtolower($produce);
    $units = strtolower($units);
    
    echo "produce: ", $produce, "<br>";
    echo "units: ", $units, "<br>";
    
    $sql = "INSERT INTO produce VALUES('$produce', '$units')";
    mysqli_query($link, $sql) or die('Error querying database.');
    
    mysqli_close($link);
?>