<!-- Start of access restriction -->
<?php
session_start();
if(strcmp($_SESSION['role'], "Administrator") != 0 && strcmp($_SESSION['role'], "Donor") != 0 && strcmp($_SESSION['role'], "Pantry") != 0) {
    // does not have permission to access this page, redirect
    header('Location: no_access.php');
    die();
} else {
    // has permission, grant access
}
require_once 'config.php';
?>
<!-- End of access restriction -->

<html>
    <head>
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    </head>
    <body>
        <div id="nav-placeholder">
        
        </div>
        <script>
            $(function(){
              $("#nav-placeholder").load("application.php");
            });
        </script>
        <div class="container-fluid">
            <div class="row">
                <?php 
                session_start();
                $sql = "";
                if($_SESSION['role'] == ("Administrator")) {
                    $sql = "SELECT * FROM donations WHERE dropped_up != '1'";
                } else if($_SESSION['role'] == ("Donor" or "Pantry"))  {
                    $id = $_SESSION['id'];
                    $sql = "SELECT * FROM donations where id = '$id'";
                }
                mysqli_query($link, $sql) or die('Error querying database.');
    
                $result = mysqli_query($link, $sql);
            
                echo '<div class="col-lg-2"></div>';
                echo '<div class="col-lg-8 justify-content-center">';
                
                    echo '<div class="row" style="margin-right: 0px; margin-left: 0px; padding-bottom:20px;">';
                        echo'<a href="export_data.php" class="btn btn-success pull-right">Export Donations</a>';
                    echo '</div>';
                    
                    echo '<table class="table table-bordered table-responsive table-striped">';
                        echo '<thead class="text-center thead-dark">
                                <tr><th class="text-center">Donor ID</th>
                                <th class="text-center">Item</th>
                                <th class="text-center">Amount</th>
                                <th class="text-center">Date</th>';
                                if($_SESSION['role'] == ("Administrator")){
                                    echo    '<th class="text-center">Modify</th>
                                    <th class="text-center">Drop Off</th>';
                                }

                        echo    '</tr></thead>';
                        echo '<tbody>';

                            while ($row = mysqli_fetch_array($result)) {
                                echo '<tr><td class="text-center" style="vertical-align: middle;">';
                                echo $row['id'];
                                echo '<td class="text-center" style="vertical-align: middle;">';
                                echo $row['produce'];
                                echo '</td>';
                                echo '<td class="text-center" style="vertical-align: middle;">';
                                echo $row['quantity'];
                                echo '</td>';
                                echo '</td><td class="text-center" style="vertical-align: middle;">';
                                echo substr($row['pick_up_date'], 0, 10);
                                echo '</td>';
                                if($_SESSION['role'] == ("Administrator")){
                                    //needed to populate dropdown menu
                                    $sql_dropdown = "SELECT * FROM users where role = 'Pantry'";
                                    mysqli_query($link, $sql_dropdown) or die('Error querying database.');
                                    $result_dropdown = mysqli_query($link, $sql_dropdown);
                                    
                                    echo '<td><div class="">';
                                    echo '<a href= "edit_donations.php?id=' . $row[id] . '&produce=' . $row[produce] . '&data_entry=' . $row[data_entry_datetime] . '"class="btn btn-primary btn-block" style="margin: 5px 0;">';
                                    echo '<span class="glyphicon glyphicon-edit" aria-hidden="true"></span>';
                                    echo '<span><strong>Edit</strong></span>';           
                                    echo '</a></div>';
                                    
                                    echo '<div class="">';
                                    echo '<a href="remove_donation.php?id=' . $row[id] . '&produce=' . $row[produce] . '&data_entry=' . $row[data_entry_datetime] . '" class="btn btn-primary btn-block">';
                                    echo '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
                                    echo '<span><strong>Delete</strong></span>  ';          
                                    echo '</a></div></td>';
                                    
                                    
                                    echo '<td class="text-center" style="vertical-align: middle;">';
                                    ?>
                                    
                                    <div class="dropdown">
                                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Pantry
                                        <span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <?php
                                            // Add options to the drop down
                                            while($row_dropdown = mysqli_fetch_array($result_dropdown))
                                            {
                                             // echo "<li><a href='" . "test_assign_donation.php?username=' . $row_dropdown['username'] . "'>" . $row_dropdown['username'] . "</a></li>";
                                              echo '<li><a href="assign_donation.php?username=' . $row_dropdown[username] . '&pantry_id=' . $row_dropdown[id] . '&donor_id=' . $row[id] . '&produce=' . $row[produce] . '&pick_up_date=' . $row[pick_up_date] . '&data_entry=' . $row[data_entry_datetime] . '&quantity=' . $row[quantity] . '">' . $row_dropdown[username] . '</a></li>';
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                    
                                    <?php
                                    echo '</td>';
                                }
                                
                                echo '</t>';
                            }
                        echo "</tbody>";

                        
                    echo "</table>";
            
                echo '</div>';
                echo '<div class="col-lg-2"></div>';
                mysqli_close($link); ?>
            </div>
        </div>
    </body>
</html>

<style type="text/css">

    table {
        
        text-align: right;
    }
</style>