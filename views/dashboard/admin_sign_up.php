<?php
// Include config file
require_once 'config.php';
 
// Define variables and initialize with empty values
$username = $password = $confirm_password = $first_name = $last_name = 
$street = $city = $state = $zip_code = $phone = $email = "";

$username_err = $password_err = $confirm_password_err = $first_name_err = $last_name_err = 
$street_err = $city_err = $state_err = $zip_code_err = $phone_err = $email_err = "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    // Validate username
    if(empty(trim($_POST["username"]))){
        $username_err = "Please enter a username.";
    } else{
        // Prepare a select statement
        $sql = "SELECT id FROM users WHERE username = ?";
        
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_username);
            
            // Set parameters
            $param_username = trim($_POST["username"]);
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                /* store result */
                mysqli_stmt_store_result($stmt);
                
                if(mysqli_stmt_num_rows($stmt) == 1){
                    $username_err = "This username is already taken.";
                } else{
                    $username = trim($_POST["username"]);
                }
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
        }
         
        // Close statement
        mysqli_stmt_close($stmt);
    }
    
    // Validate password
    if(empty(trim($_POST['password']))){
        $password_err = "Please enter a password.";     
    } elseif(strlen(trim($_POST['password'])) < 6){
        $password_err = "Password must have atleast 6 characters.";
    } else{
        $password = trim($_POST['password']);
    }
    
    // Validate confirm password
    if(empty(trim($_POST["confirm_password"]))){
        $confirm_password_err = 'Please confirm password.';     
    } else{
        $confirm_password = trim($_POST['confirm_password']);
        if($password != $confirm_password){
            $confirm_password_err = 'Password did not match.';
        }
    }
    
    // Validate first_name
    if(empty(trim($_POST['first_name']))){
        $first_name_err = "Please enter your first name.";     
    } else{
        $first_name = trim($_POST['first_name']);
    }
    
    // Validate last_name
    if(empty(trim($_POST['last_name']))){
        $last_name_err = "Please enter your last name.";     
    } else{
        $last_name = trim($_POST['last_name']);
    }
    
    // Validate organization
    /*
    if(empty(trim($_POST['organization']))){
        $organization_err = "Please enter your organization name.";     
    } else{
        $organization = trim($_POST['organization']);
    }
    */
    // Validate street
    if(empty(trim($_POST['street']))){
        $street_err = "Please enter your street address.";     
    } else{
        $street = trim($_POST['street']);
    }
    
    // Validate city
    if(empty(trim($_POST['city']))){
        $city_err = "Please enter a city.";     
    } else{
        $city = trim($_POST['city']);
    }
    
    // Validate state
    if(empty(trim($_POST['state']))){
        $state_err = "Please enter a state.";     
    } else{
        $state = trim($_POST['state']);
    }
    
    // Validate zip_code
    if(empty(trim($_POST['zip_code']))){
        $zip_code_err = "Please enter a zip code.";     
    } else{
        $zip_code = trim($_POST['zip_code']);
    }
    
    // Validate phone
    if(empty(trim($_POST['phone']))){
        $phone_err = "Please enter a phone number.";     
    } else{
        $phone = trim($_POST['phone']);
    }
    
    // Validate email
    if(empty(trim($_POST['email']))){
        $email_err = "Please enter an email.";     
    } else{
        $email = trim($_POST['email']);
    }
    
    // Check input errors before inserting in database
    if(empty($username_err) && empty($password_err) && empty($confirm_password_err) && empty($first_name_err) && empty($last_name_err) && empty($street_err) && empty($city_err) && empty($state_err) && empty($zip_code_err) && empty($phone_err) && empty($email_err) ){
        
        // Prepare an insert statement
        $sql = "INSERT INTO users (username, password, role, first_name, last_name, street, city, state, zip_code, phone, email) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
         
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "sssssssssss", $param_username, $param_password, $param_role, $param_first_name, $param_last_name,
            $param_street, $param_city, $param_state, $param_zip_code, $param_phone, $param_email);
            
            // Set parameters
            $param_username = $username;
            $param_password = password_hash($password, PASSWORD_DEFAULT); // Creates a password hash
            $param_role = 'Administrator';
            $param_first_name = $first_name;
            $param_last_name = $last_name;
            $param_street = $street;
            $param_city = $city;
            $param_state = $state;
            $param_zip_code = $zip_code;
            $param_phone = $phone;
            $param_email = $email;
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Redirect to login page
                header("location: login.php");
            } else{
                echo "Something went wrong. Please try again later.";
            }
        }
         
        // Close statement
        mysqli_stmt_close($stmt);
    }
    
    // Close connection
    mysqli_close($link);
}
?>



<html>
    <head>
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    </head>
    <body>
        <!--Navigation bar-->
        <div id="nav-placeholder">
        
        </div>
        
        <script>
            $(function(){
              $("#nav-placeholder").load("application.php");
            });
        </script>
        <!--end of Navigation bar-->
        <!-- Logo -->
        <div class="row">
          <div class="background col-md-12">
              <div class="logo">
                  <img src="/assets/images/seed.png" />
              </div>
          </div>
        </div>
        
        <div class="wrapper">
        <h2>Administrator Sign Up</h2>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
                <input placeholder="Username" type="text" name="username"class="form-control" value="<?php echo $username; ?>">
                <span class="help-block"><?php echo $username_err; ?></span>
            </div>    
            <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
                <input placeholder="Password" type="password" name="password" class="form-control" value="<?php echo $password; ?>">
                <span class="help-block"><?php echo $password_err; ?></span>
            </div>
            <div class="form-group <?php echo (!empty($confirm_password_err)) ? 'has-error' : ''; ?>">
                <input placeholder="Confirm Password" type="password" name="confirm_password" class="form-control" value="<?php echo $confirm_password; ?>">
                <span class="help-block"><?php echo $confirm_password_err; ?></span>
            </div>
            
            <div class="form-group <?php echo (!empty($first_name_err)) ? 'has-error' : ''; ?>">
                <input placeholder="First" type="text" name="first_name"class="form-control" value="<?php echo $first_name; ?>">
                <span class="help-block"><?php echo $first_name_err; ?></span>
            </div>
            
            <div class="form-group <?php echo (!empty($last_name_err)) ? 'has-error' : ''; ?>">
                <input placeholder="Last" type="text" name="last_name"class="form-control" value="<?php echo $last_name; ?>">
                <span class="help-block"><?php echo $last_name_err; ?></span>
            </div>
            
            <!-- 
            <div class="form-group <?php echo (!empty($organization_err)) ? 'has-error' : ''; ?>">
                <input placeholder="Organization" type="text" name="organization"class="form-control" value="<?php echo $organization; ?>">
                <span class="help-block"><?php echo $organization_err; ?></span>
            </div>
            -->
            
            <div class="form-group <?php echo (!empty($street_err)) ? 'has-error' : ''; ?>">
                <input placeholder="Street" type="text" name="street"class="form-control" value="<?php echo $street; ?>">
                <span class="help-block"><?php echo $street_err; ?></span>
            </div>
            <div class="form-group <?php echo (!empty($city_err)) ? 'has-error' : ''; ?>">
                <input placeholder="City" type="text" name="city"class="form-control" value="<?php echo $city; ?>">
                <span class="help-block"><?php echo $city_err; ?></span>
            </div>
            <!-- state needs to be made into a dropdown window giving only 2 character options -->
            <div class="form-group <?php echo (!empty($state_err)) ? 'has-error' : ''; ?>">
                <input placeholder="State" type="text" name="state"class="form-control" value="<?php echo $state; ?>">
                <span class="help-block"><?php echo $state_err; ?></span>
            </div>
            <div class="form-group <?php echo (!empty($zip_code_err)) ? 'has-error' : ''; ?>">
                <input placeholder="Zip Code" type="text" name="zip_code"class="form-control" value="<?php echo $zip_code; ?>">
                <span class="help-block"><?php echo $zip_code_err; ?></span>
            </div>
            <div class="form-group <?php echo (!empty($phone_err)) ? 'has-error' : ''; ?>">
                <input placeholder="Phone" type="text" name="phone"class="form-control" value="<?php echo $phone; ?>">
                <span class="help-block"><?php echo $phone_err; ?></span>
            </div>
            <div class="form-group <?php echo (!empty($email_err)) ? 'has-error' : ''; ?>">
                <input placeholder="Email" type="text" name="email"class="form-control" value="<?php echo $email; ?>">
                <span class="help-block"><?php echo $email_err; ?></span>
            </div>
            
            <div class="form-group">
                <input type="submit" class="btn btn-warning" value="Submit">
                <input type="reset" class="btn btn-default" value="Reset">
            </div>
            <p>Already have an account? <a href="login.php">Login here</a>.</p>
        </form>
    </div> 
    </body>
    
    <style type="text/css">
            .about-content {
                margin-top: 1em;
            }
            .wrapper{ width: 350px; padding: 20px; margin: 0 auto;}
        
            .background {
                background-size: cover;
                background-position: center center;
                position: relative;
                height: 8vw;
                min-height: 250px;
                width: 100%;
            }
        
            .logo {
            }
        
                .logo > img {
                    min-width: 250px;
                    width: 27%;
                    left: 50%;
                    position: absolute;
                    transform: translateX(-50%);
                }
            
            #signuprow {
            }
        
            #user {
                padding-left: 25%;
                padding-right: 25%;
                padding-top: 10px;
                text-align: center;
            }
        
            #pass {
                padding-left: 25%;
                padding-right: 25%;
                padding-top: 5px;
                padding-bottom: 5px;
                text-align: center;
            }
            #confpass {
                padding-left: 25%;
                padding-right: 25%;
                padding-top: 0px;
                padding-bottom: 5px;
                text-align: center;
            }
        
            #signupbutt {
                padding-left:47%;
            }
            #signupheader {
                text-align: center;
            }
        </style>
</html>