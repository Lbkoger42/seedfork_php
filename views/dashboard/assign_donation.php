<!-- Start of access restriction -->
<?php
session_start();
if(strcmp($_SESSION['role'], "Administrator") != 0) {
    // does not have permission to access this page, redirect
    header('Location: no_access.php');
    die();
} else {
    // has permission, grant access
}
require_once 'config.php';
?>
<!-- End of access restriction -->

<?php

if(isset($_GET['username']) && isset($_GET['pantry_id']) && isset($_GET['donor_id']) && isset($_GET['produce']) && isset($_GET['pick_up_date']) && isset($_GET['quantity']) && isset($_GET['data_entry'])){
        $username = $_GET['username'];
        $pantry_id = $_GET['pantry_id'];
        $donor_id = $_GET['donor_id'];
        $produce = $_GET['produce'];
        $pick_up_date = $_GET['pick_up_date'];
        $data_entry = $_GET['data_entry'];
        $quantity = $_GET['quantity'];
        $sql = "INSERT INTO donations (id, produce, quantity, pick_up_date, picked_up, dropped_up) VALUES ('$pantry_id', '$produce', '$quantity', '$pick_up_date', '1', '1')";
        $sql_update = "UPDATE donations set picked_up = '1', dropped_up = '1' where id = '$donor_id' AND produce = '$produce'  AND data_entry_datetime = '$data_entry'";
        mysqli_query($link, $sql) or die('Error inserting into database.');
        mysqli_query($link, $sql_update) or die('Error inserting into database.');
        header("location: view_donations.php");
}
else{
        echo "You have entered this page by accident.";
    }

?>