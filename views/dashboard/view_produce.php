<!-- Start of access restriction -->
<?php
session_start();
if(strcmp($_SESSION['role'], "Administrator") != 0 && strcmp($_SESSION['role'], "Donor") != 0) {
    // does not have permission to access this page, redirect
    header('Location: no_access.php');
    die();
} else {
    // has permission, grant access
}
?>
<!-- End of access restriction -->

<html>
    <head>
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    </head>

    <body>
        <!--Navigation bar-->
        <div id="nav-placeholder">
        
        </div>
        
        <script>
            $(function(){
              $("#nav-placeholder").load("application.php");
            });
        </script>
        <!--end of Navigation bar-->  
    </body>
</html>

<?php
    //include database configuration file
    require_once 'config.php';

    // Create connection
    $link  = new mysqli($servername, $username, $password, $database, $dbport);

    // Check connection
    if ($link ->connect_error) {
        die("Connection failed: " . $link ->connect_error);
    }
?>

<html>
 <head>
 </head>
 <body>

<?php
$sql = "SELECT * FROM produce";
mysqli_query($link, $sql) or die('Error querying database.');

$result = mysqli_query($link, $sql);


echo '<div class="col-lg-4"></div>';
echo '<div class="col-lg-4 justify-content-center">';
echo '<table class="table table-bordered table-responsive table-striped">';
echo "<tr><th>Name</th><th>Units</th><th>Modify</th></tr>";

while ($row = mysqli_fetch_array($result)) {
    echo ("<tr><td>$row[name]</td>");
    echo ("<td>$row[units]</td>");
    echo "<td><a href=\"edit_produce.php?name=$row[name]&units=$row[units]\">Edit</a>";
    echo "<a href=\"remove_produce.php?name=$row[name]&units=$row[units]\"> Delete</a></td></tr>";
}

echo "</table>";
echo '<div class="col-lg-4"></div>';


mysqli_close($link);
?>

</body>
</html>