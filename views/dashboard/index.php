<!--

Home page.

-->
<?php session_start(); ?>
<html>
    <head>
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    </head>

    <body>
        <div class="container-fluid">
        <!--Navigation bar-->
        <div id="nav-placeholder">
        
        </div>
        
        <script>
            $(function(){
              $("#nav-placeholder").load("application.php");
            });
        </script>
        <!--end of Navigation bar-->
        <div class="background col-md-12">
            <div class="logo">
                <img src="/assets/images/seed.png" />
            </div>
            <div class="statement">
                <div class="ms">
                    <h3>Growing a healthy community,<br />one seed at a time.</h3>
                </div>
            </div>
            
    
            </div>
        </div>
    </body>
    
    <style type="text/css">
    
        .background {
            background: url('/assets/images/background.jpg');
            height: 100vh;
            width: calc(100% - 30px);
            position: absolute;
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            overflow-y: hidden
            
        }
    
        .logo {}
    
        .logo > img {
                min-width: 290px;
                width: 45%;
                left: 53%;
                position: absolute;
                transform: translateX(-50%);
        }
    
        .statement {
            background-color: rgba(0, 0, 0, 0.4);
            margin-left: -15px;
            margin-right: -26px;
            height: 14vw; /*height of black box*/
            min-height: 100px; /*scaling for mobile*/
            width: 100%;
            position: absolute;
            bottom: 0;
        }
    
        .ms h3 {
            font-size: 4.15vw; /*control size of statement*/
            color: white;
            font-family: "Segoe UI Light";
            font-weight: 100;
            letter-spacing: -1px;
            text-align: center;
            opacity: 1;
        }
    </style>
</html>