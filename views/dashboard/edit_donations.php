<!-- Start of access restriction -->
<?php
session_start();
if(strcmp($_SESSION['role'], "Administrator") != 0 && strcmp($_SESSION['role'], "Donor") != 0) {
    // does not have permission to access this page, redirect
    header('Location: no_access.php');
    die();
} else {
    // has permission, grant access
}
require_once 'config.php';

if(isset($_GET['id']) && isset($_GET['produce']) && isset($_GET['data_entry'])){
    $id = $_GET['id'];
    $produce = $_GET['produce'];
    $data_entry = $_GET['data_entry'];
    
    $sql = "SELECT * FROM donations WHERE id = '$id' AND produce = '$produce' AND data_entry_datetime = '$data_entry'";
    mysqli_query($link, $sql) or die('Error querying database.');

    $result = mysqli_query($link, $sql);
    $row = mysqli_fetch_array($result);
    }
    else{
        echo "You have entered this page by accident.";
    }



?>
<!-- End of access restriction -->


<html>
    <head>
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    </head>

    <body>
        <!--Navigation bar-->
        <div id="nav-placeholder">
        
        </div>
        
        <script>
            $(function(){
              $("#nav-placeholder").load("application.php");
            });
        </script>
        <!--end of Navigation bar-->
        <div class="row">
            <div class="col-lg-4 col-md-4"></div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <form class="form-horizontal" action = "edit_donation_entry.php" method = "POST">
                    <div class="form-group">
                        <input type = "hidden" name = "id" value = "<?php echo $row[0]; ?>"><br />
                        <input type = "hidden" name = "produce" value = "<?php echo $row[1]; ?>"><br />
                        <input type = "hidden" name = "data_entry" value = "<?php echo $row[3]; ?>">
                        
                        <label for="newproduce" class="col-sm-4 control-label">Produce</label>
                        <div class="col-sm-8" style="max-width: 250px;">
                            <input type="text" class="form-control" id="newproduce" value = "<?php echo $row[1]; ?>" name = "new_produce" placeholder="Produce" style="max-width">
                        </div>
                        <label for="newquantity" class="col-sm-4 control-label">Quantitiy</label>
                        <div class="col-sm-8" style="max-width: 250px;">
                            <input type="text" class="form-control" id="newquantity" value = "<?php echo $row[2]; ?>" name = "new_quantity" placeholder="Quantity" style="max-width">
                        </div>
                        <label for="newdate" class="col-sm-4 control-label">Pickup Date</label>
                        <div class="col-sm-8" style="max-width: 250px;">
                            <input type="text" class="form-control" id="newdate" value = "<?php echo $row[4]; ?>" name = "new_date" placeholder="Pickup Date" style="max-width">
                        </div>
                        <label for="newpickedup" class="col-sm-4 control-label">Picked Up</label>
                        <div class="col-sm-8" style="max-width: 250px;">
                            <input type="text" class="form-control" id="newpickedup" value = "<?php echo $row[5]; ?>" name = "new_picked_up" placeholder="Picked Up" style="max-width">
                        </div>
                        <label for="newdroppedoff" class="col-sm-4 control-label">Dropped Off</label>
                        <div class="col-sm-8" style="max-width: 250px;">
                            <input type="text" class="form-control" id="newdroppedoff" value = "<?php echo $row[6]; ?>" name = "new_dropped_off" placeholder="Dropped Off" style="max-width">
                        </div>
                        <div class="col-sm-4" style="max-width: 250px;">
                            <input type = "submit" value = " submit ">
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-lg-4 col-md-4"></div>
        </div>
        <!--form action = "edit_donations.php" method = "POST">
            <input type = "hidden" name = "id" value = "<?php echo $row[0]; ?>"><br />
            produce: <input type = "text" name = "newproduce" value = "<?php echo $row[1]; ?>"><br />
            quantity: <input type = "text" name = "newquantity" value = "<?php echo $row[2]; ?>"><br />
            <input type = "hidden" name = "datetime" value = "<?php echo $row[3]; ?>">
            pickup date: <input type = "text" name = "newdate" value = "<?php echo $row[4]; ?>"><br />
            picked up: <input type = "text" name = "newpickedup" value = "<?php echo $row[5]; ?>"><br />
            dropped off: <input type = "text" name = "newdroppedoff" value = "<?php echo $row[6]; ?>"><br />
            <input type = "submit" value = " submit ">
        </form-->
    </body>
</html>
