#!/bin/sh

mysql-ctl cli < drop_tables.sql
mysql-ctl cli < create_tables.sql
mysql-ctl cli < populate_users.sql
mysql-ctl cli < populate_produce.sql
mysql-ctl cli < populate_donations.sql