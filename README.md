# Project Information

## Development
*If you are not using the pre-configured Cloud9 workspace, software versions can be found at the bottom of this file as a list of dependencies.*  

This project has been developed using a LAMP environment on Amazon's Cloud 9 service. The project has not been deployed and it is still in development. The quickest way to set up a workspace for development and testing will be to use Cloud9's PHP, Apache, and MySQL pre-configured workspace. Instructions on how to do this can be found below.

### Setting up a Workspace
#### Requirements
A Cloud9 account.  
An internet connection.
#### Step 1
Create a new workspace using the PHP, Apache, and MySQL pre-configured template.
#### Step 2
After opening the workspace initialize a git repository:  
```console
$ git init
```
#### Step 3
Make sure the directory you're working in is empty.  
*Delete any README.md files in the workspace by default.*  
Pull the project from Bitbucket:  
```console
$ git pull git@bitbucket.org:Lbkoger42/seedfork_php.git
```
#### Step 4
Run the create_and_populate_database.sh shell script:  
```console
$ ./create_and_populate_database.sh  
```
This should create all tables without having to edit any configuration files.  
If development and testing are being done in a workspace outside of Cloud9's preconfigured workspace, the config.php file will have to be edited along side all of the sql scripts.
#### Step 5
Run the project by clicking "Run Project" at the top of the Cloud9 workspace. To view the website you can then click on the "Preview" button and click "Preview Running Application" OR you can navigate to https://<your_workspace_name>-<your_c9_username>.c9users.io/  
  
*<your_workspace_name> = The name of your C9 workspace*  
*<your_c9_username> = Your C9 username*  
*For example, mine are seedfork_php and lbkoger42, respectively. So I would navigate to https://seedfork-php-lbkoger42.c9users.io/*  

## Credentials
#### Database
Database credentials are located in the config file
#### Usernames
adamfrisk  
admin  
brooklynfrisk  
CheerfulGiver001  
elijahduun  
example_farmer  
example_pantry  
FarmerDude  
FarmerJane  
FarmerJoe  
FarmerJohn  
FoodPantry45  
janerain  
juderain  
MisterPrez  
timkoger 
#### Passwords
Password for all users is currently "password"  

## File Structure
All php scripts and the bulk of the website is in views/dashboard/
All images and other content are in assets/
All database scripts are in the root folder of the workspace along with the README

## Database Stuff

#### Manually accessing the database
The easiest way to manually access the database in Cloud9 is to type the following into the terminal:  
```console
$ mysql-ctl cli  
```
The database used in this project is the c9 database.  
To use this database type the following into the terminal:  
```console
mysql> use c9  
```
All tables being actively used can be access here.

#### The describe command will tell you what the schema of a table looks like
Type the following into the terminal:  
```console
mysql> describe donations;
```

#### Making adjustments to the c9 tables
When an adjustment needs to be made to any tables, their respective sql scripts should be edited and the create_and_populate_database.sh script should be run.

## Software Versions
##### Apache
2.4.7
##### PHP
5.5.9
##### MySQL
5.5.57