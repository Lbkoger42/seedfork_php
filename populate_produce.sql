use c9;

DELETE FROM produce WHERE name != 'NULL';

INSERT INTO produce (name, units)
VALUES ('corn', 'flats');

INSERT INTO produce (name, units)
VALUES ('apples', 'flats');

INSERT INTO produce (name, units)
VALUES ('peaches', 'flats');

INSERT INTO produce (name, units)
VALUES ('green beans', 'flats');

INSERT INTO produce (name, units)
VALUES ('watermelon', 'flats');

INSERT INTO produce (name, units)
VALUES ('tomatoes', 'flats');

INSERT INTO produce (name, units)
VALUES ('cantaloupe', 'flats');

INSERT INTO produce (name, units)
VALUES ('cucumber', 'flats');

INSERT INTO produce (name, units)
VALUES ('pears', 'flats');
