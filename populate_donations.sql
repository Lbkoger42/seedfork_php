use c9;

DELETE FROM donations WHERE id != 0;

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('2' , 'apples', '3', '2018-03-20');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('2', 'green beans', '2', '2018-03-20');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('2' , 'cantaloupe', '4','2018-03-22');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('2' , 'peaches', '5', '2018-05-22');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('2', 'cucumber', '6', '2018-05-14');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('2' , 'corn', '7', '2018-05-16');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('3' , 'pears', '2', '2018-03-22');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('3', 'cucumber', '2', '2018-03-12');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('3' , 'tomatoes', '4', '2018-03-30');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('3', 'cantaloupe', '10', '2018-05-11');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('3', 'watermelon', '5', '2018-05-11');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('3', 'corn', '4', '2018-05-11');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('4', 'apples', '6', '2018-3-31');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('4', 'cucumber', '3', '2018-03-31');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('4', 'green beans', '4', '2018-03-31');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('4', 'tomatoes' ,'5', '2018-05-12');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('4', 'corn' ,'5', '2018-05-12');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('4', 'peaches' ,'5', '2018-05-12');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('5', 'watermelon' ,'4', '2018-03-12');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('5', 'tomatoes' ,'4', '2018-03-12');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('5', 'cucumber' ,'4', '2018-03-12');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('5', 'peaches' ,'3', '2018-05-02');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('5', 'apples' ,'2', '2018-05-02');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('5', 'green beans' ,'1', '2018-05-04');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('6', 'cantaloupe' ,'8', '2018-03-24');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('6', 'watermelon' ,'5', '2018-03-24');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('6', 'corn' ,'7', '2018-03-24');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('6', 'cucumber' ,'6', '2018-06-26');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('6', 'green beans' ,'6', '2018-06-24');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('6', 'peaches' ,'6', '2018-06-24');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('7', 'apples' ,'8', '2018-03-13');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('7', 'peaches' ,'8', '2018-03-13');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('7', 'watermelon' ,'8', '2018-03-13');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('7', 'green beans' ,'5', '2018-05-13');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('7', 'tomatoes' ,'5', '2018-05-13');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('7', 'cucumber' ,'14', '2018-05-14');

INSERT INTO donations (id, produce, quantity, data_entry_datetime, pick_up_date)
VALUES ('8', 'cantaloupe' ,'13', '2018-03,-21', '2018-03-22');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('8', 'watermelon' ,'18', '2018-03-22');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('8', 'green beans' ,'10', '2018-03-22');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('8', 'peaches' ,'5', '2018-05-23');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('8', 'corn' ,'5', '2018-05-23');

INSERT INTO donations (id, produce, quantity, data_entry_datetime, pick_up_date)
VALUES ('8', 'cantaloupe' ,'15', '2018-04-06', '2018-05-23');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('9', 'peaches' ,'8', '2018-03-23');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('9', 'corn' ,'11', '2018-03-23');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('9', 'apples' ,'9', '2018-03-23');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('9', 'cantaloupe' ,'10', '2018-05-27');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('9', 'green beans' ,'10', '2018-05-27');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('9', 'tomatoes' ,'20', '2018-05-27');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('10', 'watermelon' ,'13', '2018-03-17');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('10', 'green beans' ,'4', '2018-03-18');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('10', 'peaches' ,'6', '2018-03-19');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('10', 'corn' ,'13', '2018-05-17');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('10', 'apples' ,'9', '2018-05-17');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('10', 'cucumber' ,'21', '2018-05-14');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('11', 'peaches' ,'6', '2018-03-12');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('11', 'corn' ,'4', '2018-03-14');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('11', 'cucumber' ,'6', '2018-03-13');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('11', 'pears' ,'7', '2018-05-08');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('11', 'tomatoes' ,'12', '2018-05-07');

INSERT INTO donations (id, produce, quantity, data_entry_datetime, pick_up_date)
VALUES ('11', 'pears' ,'14', '2018-04-06', '2018-05-09');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('12', 'corn' ,'33', '2018-03-01');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('12', 'cucumber' ,'42', '2018-03-01');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('12', 'peaches' ,'24', '2018-03-01');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('12', 'green beans' ,'16', '2018-05-31');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('12', 'pears' ,'31', '2018-05-29');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('12', 'cantaloupe' ,'50', '2018-05-31');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('13', 'corn' ,'23', '2018-02-29');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('13', 'apples' ,'32', '2018-02-29');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('13', 'green beans' ,'45', '2018-02-29');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('13', 'watermelon' ,'55', '2018-06-01');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('13', 'peaches' ,'100', '2018-06-02');

INSERT INTO donations (id, produce, quantity, pick_up_date)
VALUES ('13', 'cantaloupe' ,'999', '2018-06-4');